package com.eselhat.btnsheetdemo

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView

/**
 * Create by Petter on 10/12/2019
 **/

class MyAdapter : RecyclerView.Adapter<MyAdapter.MyHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.row_item, parent, false)
        return MyHolder(view)
    }

    override fun getItemCount(): Int = 30

    override fun onBindViewHolder(holder: MyHolder, position: Int) {
        holder.itemView.setOnClickListener {
            Toast.makeText(
                holder.itemView.context,
                "Este es un click $position",
                Toast.LENGTH_SHORT
            ).show()
            Log.d("AQUI", "este es un click")
        }
    }

    class MyHolder(view: View) : RecyclerView.ViewHolder(view) {}
}